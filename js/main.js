const data = [
  {
    title: "DIGITAL AGENCIES",
    content:
      "Salt uses  Tutoom to build groundbreaking solutions for the client",
  },
  {
    title: "FREELANCERS",
    content: "Web designers build business with Tutoom",
  },
  {
    title: "NONPROFIT",
    content: "Possible makes an impact with the help of Tutoom",
  },
];

$(function () {
  data.forEach(function (element, index) {
    $("#div-images").append(`
            <div class="col-md-4">
                <div class="img" id="img-${index + 1}">
                    <p class="text-left ml-4 pt-4">${element.title}</p>
                    <div class="row ml-2">
                        <div class="col-md-6">
                            <p class="text-justify">${element.content}</p>
                            <button type="button" class="btn btn-outline-primary mt-5">Read the story →</button>
                        </div>
                    </div>
                </div>
            </div>
    `);
  });
});
